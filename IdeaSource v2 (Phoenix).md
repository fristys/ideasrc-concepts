# IdeaSource v2 (Phoenix)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/phoenix-publicdomainvectors.org.png)
/(vector image found on [PHOENIX VECTOR IMAGE - Download at  Vectorportal](https://www.vectorportal.com/stockvectors/Animals/PHOENIX-VECTOR-IMAGE/13820.aspx#))/

I really want to recreate my social network experiment and learn/use some new tech while I’m at it.

I’ve settled on using [Vue 2](vuejs.org) and the [Bulma: a modern CSS framework based on Flexbox](https://bulma.io)  _(yup, no Flatsource or Nightwing, enough with never-ending projects)_.

## Things to figure out
	- [ ] Code structure
	- [ ] Branding
		- [ ] Colours
		- [ ] Logo
		- [ ] Mascot? _(A flaming phoenix, perhaps?)_
	- [ ] Design _(this one’s a biggie)_
	- [ ] What functionality to keep and what to expand on
	- [ ] Delimitations _(maybe I shouldn’t go overly crazy with them fancy features, I want to keep this strictly minimalistic)_

## Design

### Typography

Typography makes or breaks a design.

I quite like some of the new design trends, such as the bold anti-aliased fonts, such as:

	1.  [Google Fonts - Monsterrat](https://fonts.google.com/specimen/Montserrat)
	2.  [Google Fonts - Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro)
	3. [Harmonia Sans™ - Webfont & Desktop font « MyFonts](https://www.myfonts.com/fonts/mti/harmonia-sans/)
	4. The “default OS font stack”:
```css
	.default-font-stack {
		font-family: BlinkMacSystemFont, -apple-system, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
	}
```


### Landing page

Something, something, first impressions, something.

I like landing page designs, such as these below. _(I’ve noticed a trend of me liking pages where there are bright colours, sectioned off presentation of content, and…vector images, how the hell do I draw those, I can’t even draw a straight line…)_

1. [The Best Laravel and PHP Screencasts](https://laracasts.com/)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/The%20Best%20Laravel%20and%20PHP%20Screencasts.png)

2. [Your immortal AI Doppelganger](https://uploadme.ai/)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/Your%20immortal%20AI%20Doppelganger.png)

3. [Your personal recruiting concierge. Use the expertise of hundreds of specialized staffing services &mdash all through one user interface. – Gustav](https://hellogustav.com/)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/Your%20personal%20recruiting%20concierge.%20Use%20the%20expertise%20of%20hundreds%20of%20specialized%20staffing%20services%20%20mdash%20all%20through%20one%20user%20interface.%20%E2%80%93%20Gustav.png)

4. [Video Hosting for Business | Wistia](https://wistia.com/)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/Video%20Hosting%20for%20Business%20%20%20Wistia.png)

5. [Debt Relief Program & Solutions | 800-893-3123](https://www.freedomdebtrelief.com/) _(Yeah, I know, right?)_
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/Debt%20Relief%20Program%20%20%20Solutions%20%20%20800%20893%203123.png)

6. [Employee engagement and performance management software - Teambit](https://teambit.io/) _(I LOVE how clean this one looks)_
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/Employee%20engagement%20and%20performance%20management%20software%20%20%20Teambit.png)

7. [Digital Marketing Agency :: Web Profits :: Australia](https://www.webprofits.com.au/) _(this one has a video background header much like the original IdeaSource)_
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/Digital%20Marketing%20Agency%20%20%20%20Web%20Profits%20%20%20%20Australia.png)

8. [Create your online store today with Shopify](https://www.shopify.com/free-trial) _(nice. seamless. simple. make-up. Wait…wrong channel)_
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/Create%20your%20online%20store%20today%20with%20Shopify.png)


### Actual application

Sigh..I’m not even sure what functionality I want in this app, but I’m just going to paste some stuff of Dribble that I dig, and go from there.

![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/Landingpage-812006.png)

![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/facebook-desktop_search_buttn_.png)

![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/01profile-full.png)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/01profile.png)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/02feed.png)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/03messages.png)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/contacts-real-pixels.png)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/details-real-pixels.png)
![a](https://bytebucket.org/fristys/ideasrc-concepts/raw/master/IdeaSource%20v2%20%28Phoenix%29/post-real-pixels.png)



